class Node
    attr_accessor :be, :ed, :next
    def initialize(be, ed)
        @be = be
        @ed = ed
        @next = nil
    end

    def printn
        print "[#@be, #@ed) "
    end
end

class RangeList
    attr_accessor :tail, :head
    def initialize
        @tail = nil
        @head = Node.new(-1, -1)
    end
    def add(arr)
        nbe = arr[0]
        ned = arr[1]
        p = @head
        if p.next == nil
            p.next = Node.new(nbe, ned)
            return
        end
        while p.next != nil && p.next.ed < nbe
            p = p.next
        end
        # 此时p指向尾节点或 与p重叠的第一个区间节点之前的那个节点
        if p.next == nil
            p.next = Node.new(nbe, ned)
            return
        end

        p = p.next
        p.be = p.be < nbe ? p.be : nbe
        p.ed = p.ed > ned ? p.ed : ned

        # 合并重叠的区间
        while p.next != nil && p.next.be < p.ed
            if p.next.ed > p.ed
                p.ed = p.next.ed
                p.next = p.next.next
            else
                p.next = p.next.next
            end
        end 
    end
    def remove(arr)
        rbe = arr[0]
        red = arr[1]
        if rbe >= red 
            return 
        end
        p = @head
        while p.next != nil
            if p.next.ed > rbe # 进入可能被删除的区域
                if p.next.be > red # 起点比可删除区间的终点大，此节点及之后节点均不删除
                    break 
                elsif p.next.be >= rbe && p.next.ed <= red # 节点区间是待删除区间的子区间，直接删除这个节点区间
                    p.next = p.next.next
                elsif p.next.be < rbe && p.next.ed > red # 待删除区间是节点区间的子区间，需要新增节点区间
                    oldnext = p.next.next
                    p.next.next = Node.new(red, p.next.ed)
                    p.next.ed = rbe
                    p.next.next.next = oldnext
                    break;
                elsif p.next.ed > red # 节点区间的前半部分需要删除
                    p.next.be = red
                    break
                elsif p.next.be < rbe # 节点区间的后半部分需要删除，且节点区间之后的区间可能也要删除
                    p.next.ed = rbe
                    p = p.next
                end

            else
                p = p.next
            end
        end
    end
    def print
        p = @head
        while p.next
            p.next.printn
            p = p.next
        end
        puts ""
    end
end

rl = RangeList.new
rl.add([1, 5])
rl.print
# // Should display: [1, 5)
rl.add([10, 20])
rl.print
# // Should display: [1, 5) [10, 20)
rl.add([20, 20])
rl.print
# // Should display: [1, 5) [10, 20)

rl.add([20, 21])
rl.print
# // Should display: [1, 5) [10, 21)
rl.add([2, 4])
rl.print
# // Should display: [1, 5) [10, 21)
rl.add([3, 8])
rl.print
# // Should display: [1, 8) [10, 21)
rl.remove([10, 10])
rl.print
# // Should display: [1, 8) [10, 21)
rl.remove([10, 11])
rl.print
# // Should display: [1, 8) [11, 21)
rl.remove([15, 17])
rl.print
# // Should display: [1, 8) [11, 15) [17, 21)
rl.remove([3, 19])
rl.print
# // Should display: [1, 3) [19, 21)


# Reference:
# https://dev.to/mwong068/introduction-to-linked-lists-in-ruby-kgi
